package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func concatDirName(parent, child string) string {
	return parent + string(os.PathSeparator) + child
}

func hasDir(dirName string) (bool, error) {
	dir, err := ioutil.ReadDir(dirName)
	if err != nil {
		return false, err
	}

	for _, file := range dir {
		if file.IsDir() {
			return true, nil
		}
	}
	return false, nil
}

func selectDir(dir []os.FileInfo) []os.FileInfo {
	nDir := 0
	for _, file := range dir {
		if file.IsDir() {
			nDir++
		}
	}

	selectedDir := make([]os.FileInfo, nDir)
	index := 0
	for _, file := range dir {
		if file.IsDir() {
			selectedDir[index] = file
			index++
		}
	}

	return selectedDir
}

func printNode(isLastNode []bool) {
	for _, isLast := range isLastNode {
		if isLast {
			fmt.Print("| ")
		} else {
			fmt.Print("  ")
		}
	}
	fmt.Print("+-")
}

func printDir(parent string, isLastNode []bool) error {
	dir, err := ioutil.ReadDir(parent)
	if err != nil {
		return err
	}
	dir = selectDir(dir)

	for id, file := range dir {
		printNode(isLastNode)
		fmt.Println(file.Name())

		nextDirName := concatDirName(parent, file.Name())
		nextIsLastNode := append(isLastNode, id != len(dir) -1)
		err := printDir(nextDirName, nextIsLastNode)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {
	targetDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	if 1 < len(os.Args) {
		targetDir = os.Args[1]
		_, err = os.Stat(targetDir)
		if os.IsNotExist(err) {
			fmt.Println("The directory does not exist")
			os.Exit(1)
		}
	}

	canSearch, err := hasDir(targetDir)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if !canSearch {
		fmt.Println("The directory does not have a child directory")
		os.Exit(1)
	}

	err = printDir(targetDir, make([]bool, 0))
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
